create table bestellposition (
  fk_auftrag int(10) unsigned,
  position tinyint,
  fk_artikel smallint(5) unsigned,
  anzahl int,
  foreign key(fk_auftrag) references auftrag(auft_nr) on update cascade on delete set null,
  foreign key(fk_artikel) references artikel(art_nr)  on update cascade on delete cascade
);

load data infile "/repo/importe/bestellungimp.txt" into table bestellposition;