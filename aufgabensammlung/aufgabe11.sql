create view kunden_nach_ort as
  select * from kunde where ort = 'essen';

select count(*) from kunden_nach_ort where vorname = 'hans';

create or replace view kunden_nach_ort as 
  select * from kunde where ort = 'frankfurt';

show tables;

drop view kunden_nach_ort;