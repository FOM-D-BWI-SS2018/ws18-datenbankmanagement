-- Klausur

-- In der Klausur sind 4 Abfragen mit 24 Punkte
-- In der Klausur sind die Aufgabenstellungen sehr kurz gehalten
-- mindestens ist tipp für distinct
-- Bei mp sind keine Minuspunkte, aber Teilpunkte und Anzahl der korrekten Lösungsmöglichkeiten sind gegeben

-- 30 Punkte Theorie
  -- Kein ER Modell erstellen
  -- Modelle können dran kommen mit Fehlern
  -- Alles aus der Theorie kann avorkommen
  -- In Theorie kommt ein Praxistransfer von 9 Punkte dran über "Transaktionen" dran und dann der Bezug ins Unternehmen, siehe Skript
  -- Normalisierung 3. Grades ca. 12 Punkte
    -- Es geht nur um Felder / Strukturen nicht um die Daten
  -- 4 allgemeine offene Fragen vllt auch eine MP
    -- immer in Stichpunkten antworten

-- 60 Punkte Praxis
  -- Teil 1 Abfragen 24 Punkte
    -- alle 6 Punkte für jeden Schwierigkeitsgrad
  -- Teil 2 Allgemeine Fragen 12 Punkte
    -- 4 Fragen offen
    -- erste Frage ist kleiner Bonus und bezieht sich auf das Deckblatt
  -- Teil 3 DDL 6 Punkte
    -- Statement schreiben oder Fehler erkennen / verbessern
  -- Teil 4 Multiple Choice zu allem Praktischen 18 Punkte
    -- Aufgaben genau lesen!
    -- ca 5 Stück
    -- z.b. wie werden datumswerte gespeichert
  
  -- Datenbank Struktur ziemlich gleich
    -- Shop, Shoptyp & Stadt fällt weg
    -- Versandtabelle kommt hinzu

-----------------------------------------------------------------------------------
-- Einfache Abfragen
-- 1
select * from kunde where ort = 'Muenchen';

-- 2
select * from artikel a
left join kategorie k on k.kat_nr = a.fk_kategorie
where k.kategoriebezeichnung = 'Monitor'
order by einzelpreis;

-- 3
select * from kunde where plz like '4%' order by plz asc;

-----------------------------------------------------------------------------------
-- Abfragen mit Join
-- 4
select distinct kat.kategoriebezeichnung from kunde kd
join auftrag auf on kd.kd_nr = auf.fk_kunde
join bestellposition b on auf.auft_nr = b.fk_auftrag
join artikel art on b.fk_artikel = art.art_nr
join kategorie kat on kat.kat_nr = art.fk_kategorie
where kd.kd_nr = 319;

-- 5
select art.artikelbezeichnung, auf.bestelldat from kunde kd
join auftrag auf on kd.kd_nr = auf.fk_kunde
join bestellposition b on auf.auft_nr = b.fk_auftrag
join artikel art on b.fk_artikel = art.art_nr
where kd.kd_nr = 111;

-----------------------------------------------------------------------------------
-- Abfragen mit Gruppenfunktionen
-- 6
select COUNT(distinct kd.kd_nr) from kunde kd
inner join auftrag auf on kd.kd_nr = auf.fk_kunde
where YEAR(auf.bestelldat) = 2014 AND MONTH(auf.bestelldat) = 8;
-- alternative
select COUNT(distinct fk_kunde) from auftrag  
where YEAR(bestelldat) = 2014 AND MONTH(bestelldat) = 8;
-- WICHTIG: DISTINCT geht auch in Gruppenfunktionen

-- 7
select SUM(b.anzahl * art.einzelpreis) from kunde kd
inner join auftrag auf on kd.kd_nr = auf.fk_kunde
inner join bestellposition b on auf.auft_nr = b.fk_auftrag
inner join artikel art on b.fk_artikel = art.art_nr
where kd.ort = 'essen' AND LEFT(kd.plz, 2) = '45';

-----------------------------------------------------------------------------------
-- Spitzenwertabfrage
-- 8
select MAX(anzahl), fk_auftrag from bestellposition where fk_artikel = 555;

-- only full group by !

select fk_auftrag, anzahl 
from bestellposition
where fk_artikel = 555
order by anzahl desc
limit 1;

-- 9
select LEFT(plz, 2) as PLZ, COUNT(*) as ANZAHL from kunde
group by LEFT(plz, 2)
order by COUNT(*) DESC
LIMIT 3;

-----------------------------------------------------------------------------------
-- Abfragen mit Sub-select
-- 10
select * from kunde 
where geburtsdatum = (
  select MAX(geburtsdatum) from kunde
);

-- 11
select distinct fk_kunde from auftrag 
where bestelldat = (
  select MAX(bestelldat) from auftrag where fk_kunde = 400
) and fk_kunde != 400;

-----------------------------------------------------------------------------------
-- Vermischte Abfragen

-- 12 -> typische klausuraufgabe (6 Minuten / Punkte)
select LEFT(kd.plz, 1) as PlzBereich, SUM(b.anzahl * art.einzelpreis) as umsatz 
from kunde kd
join auftrag auf on kd.kd_nr = auf.fk_kunde
join bestellposition b on auf.auft_nr = b.fk_auftrag
join artikel art on b.fk_artikel = art.art_nr
group by LEFT(kd.plz, 1)
order by umsatz asc;

-- 13 -> typische klausuraufgabe (6 Minuten / Punkte) wahrscheinlichkeit > 50%
select * from auftrag
left join bestellposition on auft_nr = fk_auftrag
where fk_auftrag IS NULL;

-- 14 -> nicht in Klausur, da Aufgabenstellung zu lang
select distinct kd_nr, vorname, nachname from kunde
inner join auftrag on kd_nr = fk_kunde
inner join bestellposition on fk_auftrag = auft_nr
inner join artikel on art_nr = fk_artikel
where fk_kategorie = (
  select kat_nr from kategorie where kategoriebezeichnung = "Festplatte"
) and anzahl > 10;

-- 15 -> nicht in Klausur
select count(*) as Kundenanzahl from kunde where LEFT(plz, 1) = '5';

-- 16 -> bisschen wichtiger für Klausur
select count(*) from artikel
inner join kategorie on fk_kategorie = kat_nr
where kategoriebezeichnung in ('Monitor', 'Drucker', 'Festplatte');

-- 17 -> kann sein
select kat_nr, kategoriebezeichnung, count(*) from artikel
inner join kategorie on fk_kategorie = kat_nr
group by kat_nr, kategoriebezeichnung;

-- 18
select max(bestelldat) from auftrag where fk_kunde = 33;

-- 19 -> 
select kd_nr, vorname, nachname, geburtsdatum from kunde 
where geburtsdatum is not null -- <<<<< irrelevant für klausur
order by geburtsdatum asc limit 10;

-- 20
select fk_auftrag, position, max(anzahl * einzelpreis) as umsatz from bestellposition 
join artikel on fk_artikel = art_nr 
group by fk_auftrag, anzahl * einzelpreis, position
order by max(anzahl * einzelpreis) desc 
limit 1;
-- hier macht die gruppierung keinen sinn mehr! Also:
select fk_auftrag, position, anzahl * einzelpreis as umsatz from bestellposition 
join artikel on fk_artikel = art_nr 
order by umsatz desc 
limit 1;

-- 21
select count(distinct fk_kunde) from auftrag 
where zahleingang is null;

-- 22
select count(distinct fk_stadt) from shop
where fk_shoptyp = (select typ_nr from shoptyp where shoptyp = "Grosshandel");
-- alternativ:
select count(distinct fk_stadt) from shop
inner join shoptyp on fk_shoptyp = typ_nr
where shoptyp = "Grosshandel";

select count(*) 
from stadt
where stadt_nr IN ( 
  select fk_stadt 
  from shop 
  where fk_shoptyp = (
    select shop_nr from shoptyp where shoptyp = 'grosshandel'
  )
);

-- 23 -> normale aufgabe in klausur 
select auft_nr, sum(anzahl * einzelpreis) as umsatz from auftrag
join bestellposition on auft_nr = fk_auftrag
join artikel on art_nr = fk_artikel
where year(lieferdat) = 2015
group by auft_nr
order by umsatz asc
limit 1;

-- 24
select distinct kd_nr, vorname, nachname from kunde 
join auftrag on fk_kunde = kd_nr
where year(bestelldat) = 2014 and month(bestelldat) = 8 and month(lieferdat) = 8;

-- 25
select distinct kd_nr, vorname, nachname from kunde
join auftrag on fk_kunde = kd_nr
where month(bestelldat) = 7 and year(bestelldat) = 2014;

-- 26
select LEFT(plz, 1) as 'plz bereich', avg(datediff(lieferdat, bestelldat)) as 'avg lieferzeit' from kunde 
join auftrag on fk_kunde = kd_nr
where plz like '4%' or plz like '8%'
group by LEFT(plz, 1);

-- 27 -> ähnlich in Klausur möglich
select distinct kd_nr, vorname, nachname from kunde 
join auftrag on fk_kunde = kd_nr
where (ort = 'hamburg' or ort = 'muenchen') and month(bestelldat) = month(lieferdat);

-- 28 -> nicht in Klausur
select * from kunde where month(geburtsdatum) = 9;

-- 29
select * from kunde where month(geburtsdatum) = (month(CURDATE()) + 1) % 12; -- oder date_add now + interval 1 month

-- 30
select ort, count(ort) from kunde 
where rabatt = 0
group by ort;

-- 31
select * from kunde where soundex(nachname) = soundex('meier');

-- 32
select count(*) from auftrag where datediff(lieferdat, bestelldat) <= 10;

-- 33
select fk_shop as shop_nr, shoptyp, strasse, plz, stadt, sum(anzahl*einzelpreis) as umsatz from auftrag
join bestellposition on auft_nr = fk_auftrag
join artikel on art_nr = fk_artikel
join kategorie on kat_nr = fk_kategorie
join shop on shop_nr = fk_shop
join stadt on stadt_nr = fk_stadt
join shoptyp on typ_nr = fk_shoptyp
where kategoriebezeichnung = 'Festplatte'
group by fk_shop, shoptyp, strasse, plz, stadt
order by umsatz desc
limit 1;
-- bis hier zählen alle aufgaben zu leicht oder mittel, ab hier auch schwer

-- 34 -> schwerste kategorie die drankommen kann
select plzbereich, avg(auftragsgewicht) from ( 
  select LEFT(kd.plz, 1) as plzbereich, sum(be.anzahl * ar.gewicht) as auftragsgewicht from auftrag au
  join bestellposition be on au.auft_nr = be.fk_auftrag
  join artikel ar on ar.art_nr = be.fk_artikel
  join kunde kd on kd.kd_nr = au.fk_kunde
  group by au.auft_nr, LEFT(kd.plz, 1) 
) sub
group by plzbereich
order by auftragsgewicht;

select LEFT(kd.plz, 1) as plzbereich, sum(be.anzahl * ar.gewicht) / count(distinct au.auft_nr) as durchschnittsgewicht from auftrag au
join bestellposition be on au.auft_nr = be.fk_auftrag
join artikel ar on ar.art_nr = be.fk_artikel
join kunde kd on kd.kd_nr = au.fk_kunde
group by LEFT(kd.plz, 1)
order by auftragsgewicht;

-- 35 -> solche gibt es auch
select ort, avg(datediff(lieferdat, bestelldat)) as lieferzeit from auftrag
join kunde on kd_nr = fk_kunde
group by ort
order by lieferzeit asc
limit 5;

-- 36 -> typisch, aber einfach / mittel -> having
select auft_nr, sum(anzahl * einzelpreis) * (1 - rabatt) as 'umsatz inkl. rabatt' from auftrag
join bestellposition on auft_nr = fk_auftrag
join artikel on art_nr = fk_artikel
join kunde on kd_nr = fk_kunde
where auft_nr >= 8000 and auft_nr <= 9000
group by auft_nr, rabatt
having sum(anzahl * einzelpreis) * (1 - rabatt)  >= 1000000;

-- 37
select count(*) from kunde where geburtsdatum is null;

-- 38
select * from (
  select auft_nr, sum(anzahl*einzelpreis) as umsatz from auftrag
  join bestellposition on auft_nr = fk_auftrag
  join artikel on art_nr = fk_artikel
  group by auft_nr
  order by umsatz desc
  limit 5
) umsaetze
order by umsatz asc
limit 1;

-- 39 -> union ist nicht klausurrelevant
select * from (
  (
    select auft_nr, sum(anzahl*einzelpreis) as umsatz from auftrag
    join bestellposition on auft_nr = fk_auftrag
    join artikel on art_nr = fk_artikel
    group by auft_nr
    order by umsatz desc
    limit 3

  ) UNION (

    select auft_nr, sum(anzahl*einzelpreis) as umsatz from auftrag
    join bestellposition on auft_nr = fk_auftrag
    join artikel on art_nr = fk_artikel
    group by auft_nr
    order by umsatz asc
    limit 3
  )
) umsaetze
order by umsatz desc;

-- 40
select * from kunde where nachname like '%r' and ( nachname like 'M%' or nachname like 'N%' or nachname like 'O%' );

-- 41
select * from kunde where dayname(geburtsdatum) = 'sunday' and (ort = 'berlin' or ort = 'hamburg');

-- 42
select kd_nr, sum(anzahl * einzelpreis) as umsatz from kunde 
join auftrag on kd_nr = fk_kunde
join bestellposition on auft_nr = fk_auftrag
join artikel on art_nr = fk_artikel
where ort = 'essen'
group by kd_nr
having sum(anazhl * einzelpreis) > 2000000; -- im having niemals aliase nutzen

-- 43
select kd_nr from kunde 
join auftrag on kd_nr = fk_kunde
where year(bestelldat) = 2014 and month(bestelldat) = 12
group by kd_nr 
having count(kd_nr) > 2;

-- 44 - non equi join nicht in klausur(nur bei alternativer lösung in den vorgegeben lösungen) -> nicht in klausur
select vorname, nachname, ort from kunde
group by vorname, nachname, ort
having count(*) >= 2;

-- 45 -> alternative bei 22
select (
  (
    select count(*) from stadt
  ) - (
    select count(distinct fk_stadt) from shop where fk_shoptyp = (select typ_nr from shoptyp where shoptyp = "Grosshandel")
  )
) as staedte_ohne_grosshandel;

select count(*) 
from stadt
where stadt_nr not in ( 
  select fk_stadt 
  from shop 
  where fk_shoptyp = (
    select shop_nr from shoptyp where shoptyp = 'grosshandel'
  )
);


-- 46 -> typisch in klausur -> nochmal die offizielen lösungen angucken
select * from bestellposition where fk_auftrag is null;

-- 47 -> schwerste kategorie -> nochmal die offizielen lösungen angucken
select abs(
  (
    select sum(anzahl*einzelpreis) as umsatz from auftrag
    join bestellposition on auft_nr = fk_auftrag
    join artikel on art_nr = fk_artikel
    where fk_shop = 15
  ) - (
    select sum(anzahl*einzelpreis) as umsatz from auftrag
    join bestellposition on auft_nr = fk_auftrag
    join artikel on art_nr = fk_artikel
    where fk_shop = 10
  )
) as umsatzdifferenz;

-- 48
select avg(umsatz) from (
  select auft_nr, sum(anzahl * einzelpreis) as umsatz from auftrag 
  join bestellposition on auft_nr = fk_auftrag
  join artikel on art_nr = fk_artikel
  where auft_nr >= 2000 and auft_nr <= 2100
  group by auft_nr
) umsaetze;

-- 49


-- 50 -- nicht in klausur
select kd_nr, vorname, nachname, geburtsdatum, year(from_days(datediff(curdate(),geburtsdatum))) as 'alter' from kunde;

-- 51 -- siehe offizielle lösung, deswegen schwerste kategorie in klausur
select left(plz, 1) from kunde
group by left(plz, 1)
having avg(year(from_days(datediff(curdate(),geburtsdatum)))) > (
  select avg(year(from_days(datediff(curdate(),geburtsdatum)))) as 'alter' from kunde
);

-- 52
select max(avg) - min(avg) as diff from (
  select avg(datediff(lieferdat, bestelldat)) as avg from auftrag
  join kunde on kd_nr = fk_kunde
  group by left(plz, 1)
) _;

-- 53 nicht klausurrelevant
select bereich, bereichAvg, gesamtAvg, abs(bereichAvg - gesamtAvg) as diff from (
  select left(plz, 1) as bereich, avg(datediff(lieferdat, bestelldat)) as bereichAvg, gesamtAvg from auftrag
  join shop on shop_nr = fk_shop
  join (select avg(datediff(lieferdat, bestelldat)) as gesamtAvg from auftrag) _
  group by left(plz, 1)
) _
order by diff asc
limit 1;

-- 54 - nicht so hohe wahrscheinlichkeit
select auft_nr, count(fk_auftrag) from auftrag
left join bestellposition on auft_nr = fk_auftrag
where auft_nr >= 8533 and auft_nr <= 8537
group by auft_nr;

-- 55


-- 56
select group_concat(auft_nr) from auftrag where fk_kunde = 7;
