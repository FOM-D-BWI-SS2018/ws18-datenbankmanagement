use bestellungen;

alter table kunden add rabatt float(3,2) default 0.03;

alter table kunden add letzter timestamp;

alter table kunden change strasse strasse varchar(35);

alter table kunden change rabatt rabatt double(3,2) default 0.03;

alter table kunden change letzter letzter_Zugriff timestamp;

alter table kunden rename to kunde;