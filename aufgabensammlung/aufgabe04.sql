insert into kunde set 
  vorname = "Claudia",
  nachname = "Schulze",
  strasse = "Hansastr. 334",
  plz = "20332",
  ort = "Hamburg",
  rabatt = 0.04,
  vorwahl = "030",
  telefon = "223234";

insert into kunde set 
  vorname = "Hans",
  nachname = "Müller",
  strasse = "Hauptstr. 44a",
  plz = "45147",
  ort = "Essen",
  rabatt = 0.03,
  vorwahl = "0201",
  telefon = "345123",
  geburtsdatum = "1967-05-20",
  ledig = 1;

insert into stadt values (1, "Hamburg", 10.0, 53.55), (2, "Duesseldorf",6.7667,51.2167), (3, "Dortmund", 4.4734, 51.5147);
insert into shoptyp values (1, "Grosshandel"), (2, "Supermarkt"), (3, "Einzelhandel");

insert into shop (shop_nr,fk_shoptyp,strasse,plz,fk_stadt) values
  (1,3, "Hauptstr. 38", "44145", 3),
  (2,1, "Lessinggasse 15", "40589", 2);

insert into auftrag set
  auft_nr = 1000,
  fk_kunde = 1,
  bestelldat = '2013-07-01',
  lieferdat = '2013-08-31',
  zahleingang = null,
  mahnung = '1',
  fk_shop = 2;

insert into auftrag set
  auft_nr = 1001,
  fk_kunde = 2,
  bestelldat = '2013-07-01',
  lieferdat = '2013-07-19',
  zahleingang = "2013-08-05",
  zahlungsziel = "2013-08-20",
  fk_shop = 1;

insert into kategorie values
  (1 ,  "Festplatte"),
  (6 ,  "Grafikkarten"),
  (2 ,  "Monitor"),
  (7 ,  "Mainboard"),
  (3 ,  "Rechner"),
  (8 ,  "Software"),
  (4 ,  "Drucker"),
  (9 ,  "Software (Anwendung)"),
  (5 ,  "Multimedia"),
  (10,  "Software (Spiel)"),
  (11,  "Testkategorie");

insert into hersteller values 
  (1,"Samsung"),
  (2,"Jamaha");

insert into artikel values
(1, "Festplatte 178 GB, 7150 rpm", 133.50, 0.8, 1, 1),
(2, "Festplatte 165 GB, 7200 rpm", 128.70, 0.82, 1, 2),
(3, 'Monitor 19" TFT XA33-5', 368.99, 11.4,2,1);