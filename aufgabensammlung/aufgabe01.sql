create database bestellungen;

use bestellungen;

create table kunden(
  kd_nr smallint unsigned zerofill AUTO_INCREMENT,
  vorname varchar(30),
  nachname varchar(30),
  strasse varchar(25),
  plz varchar(10),
  ort varchar(30),
  vorwahl varchar(10),
  telefon varchar(15),
  geburtstag date,
  ledig bool,
  PRIMARY KEY (kd_nr)
);

create table auftrag(
  auft_nr int unsigned,
  bestelldat date,
  lieferdat date,
  zahlungsziel date,
  zahleingang date,
  mahnung enum('0','1','2','3') default '0',
  PRIMARY KEY (auft_nr)
);

create table artikel(
  art_nr smallint unsigned,
  artikelbezeichnung varchar(30),
  einzelpreis double(8,2),
  gewicht_in_kg double(5,2),
  PRIMARY KEY (art_nr)
);

create table hersteller(
  herst_nr tinyint unsigned,
  herstellerbezeichnung varchar(30),
  PRIMARY KEY (herst_nr)
);

create table kategorie(
  kat_nr tinyint unsigned,
  kategoriebezeichnung varchar(30),
  PRIMARY KEY (kat_nr)
);