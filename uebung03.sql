use fom;
create table uebung03(
  wert_01 tinyint,
  wert_02 tinyint unsigned,
  wert_03 smallint,
  wert_04 smallint unsigned,
  wert_05 mediumint,
  wert_06 int,
  wert_07 bigint
);

insert into uebung03 ( wert_01, wert_02, wert_03, wert_04, wert_05, wert_06, wert_07 ) values (-10,-10,-10,-10,-10,-10,-10);
insert into uebung03 ( wert_01, wert_02, wert_03, wert_04, wert_05, wert_06, wert_07 ) values (300,300,300,300,300,300,300);
insert into uebung03 ( wert_01, wert_02, wert_03, wert_04, wert_05, wert_06, wert_07 ) values (35000,35000,35000,35000,35000,35000,35000);
insert into uebung03 ( wert_01, wert_02, wert_03, wert_04, wert_05, wert_06, wert_07 ) values (5000000,5000000,5000000,5000000,5000000,5000000,5000000);
insert into uebung03 ( wert_01, wert_02, wert_03, wert_04, wert_05, wert_06, wert_07 ) values (17.7,17.7,17.7,17.7,17.7,17.7,17.7);