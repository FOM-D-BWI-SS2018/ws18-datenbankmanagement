-- ********************************************
-- ** Vorbereitung: Setze den SQL-Modus      **
-- ** auf ONLY_FULL_GROUP_BY. Das bedeutet:  **
-- ** ungültige GROUP BY Statements sind     **
-- ** nicht mehr möglich.                    **
-- ** Wird diese Option nicht gesetzt wäre   **
-- ** es beispielsweise möglich, folgendes   **
-- ** Statement abzusetzen:                  **
-- ** SELECT ort, COUNT(*) FROM kunde;       **
-- ********************************************
SET SESSION SQL_MODE = 'STRICT_ALL_TABLES,ONLY_FULL_GROUP_BY'; 

-- ****************************************
-- ** Vorbereitung: Lösche die Datenbank **
-- ** "bestellungen", wenn Sie existiert **
-- ****************************************
DROP DATABASE IF EXISTS bestellungen;



-- ****************
-- ** AUFGABE 01 **
-- ****************

-- ******************************
-- ** Erstellung der Datenbank **
-- ******************************
CREATE DATABASE bestellungen;

-- *******************************
-- ** Aktivierung der Datenbank **
-- *******************************
USE bestellungen;

-- ***********************************
-- ** Erstellung der Tabelle kunden **
-- ***********************************
CREATE TABLE kunden (
  kd_nr SMALLINT UNSIGNED ZEROFILL AUTO_INCREMENT,
  vorname VARCHAR(30),
  nachname VARCHAR(30),
  strasse VARCHAR(25),
  plz CHAR(5),
  ort VARCHAR(30),
  vorwahl VARCHAR(6),
  telefon VARCHAR(15),
  geburtsdatum DATE,
  ledig BOOL,
  PRIMARY KEY (kd_nr)
) ENGINE=INNODB;

-- ************************************
-- ** Erstellung der Tabelle auftrag **
-- ************************************
CREATE TABLE auftrag (
  auft_nr INT UNSIGNED,
  bestelldat DATE,
  lieferdat DATE,
  zahlungsziel DATE,
  zahleingang DATE,
  mahnung ENUM ("0", "1", "2", "3") DEFAULT "0",
  PRIMARY KEY (auft_nr)
) ENGINE=INNODB;

-- ************************************
-- ** Erstellung der Tabelle artikel **
-- ************************************
CREATE TABLE artikel (
  art_nr SMALLINT UNSIGNED,
  artikelbezeichnung VARCHAR(100),
  einzelpreis DOUBLE(8,2),
  gewicht DOUBLE(5,2),
  PRIMARY KEY (art_nr)
) ENGINE=INNODB;

-- ***************************************
-- ** Erstellung der Tabelle hersteller **
-- ***************************************
CREATE TABLE hersteller (
  herst_nr TINYINT UNSIGNED,
  herstellerbezeichnung VARCHAR(30),
  PRIMARY KEY (herst_nr)
) ENGINE=INNODB;

-- **************************************
-- ** Erstellung der Tabelle kategorie **
-- **************************************
CREATE TABLE kategorie (
  kat_nr TINYINT UNSIGNED,
  kategoriebezeichnung VARCHAR(30),
  PRIMARY KEY (kat_nr)
) ENGINE=INNODB;



-- ****************
-- ** AUFGABE 02 **
-- ****************

ALTER TABLE kunden
  ADD rabatt DOUBLE(2,2) DEFAULT 0.03,
  ADD letzter TIMESTAMP,
  MODIFY strasse VARCHAR(35),
  RENAME kunde;

-- ******************************************************************
-- ** Hinweis: diese Änderung kann nicht in einem Zug mit den      **
-- ** vorhergehenden Änderungen durchgeführt werden, da das        ** 
-- ** Feld "letzter" erst angelegt werden muss.                    **
-- ******************************************************************
ALTER TABLE kunde
  CHANGE letzter letzter_Zugriff TIMESTAMP;




-- ****************
-- ** AUFGABE 03 **
-- ****************

ALTER TABLE auftrag
  ADD fk_kunde SMALLINT UNSIGNED ZEROFILL,
  ADD CONSTRAINT fk_kunde FOREIGN KEY (fk_kunde) 
    REFERENCES kunde (kd_nr)
    ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE artikel
  ADD fk_kategorie TINYINT UNSIGNED,
  ADD fk_hersteller TINYINT UNSIGNED,
  ADD CONSTRAINT fk_kategorie FOREIGN KEY (fk_kategorie) 
    REFERENCES kategorie (kat_nr)
    ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT fk_hersteller FOREIGN KEY (fk_hersteller) 
    REFERENCES hersteller (herst_nr)
    ON UPDATE CASCADE ON DELETE SET NULL;

-- ************************************
-- ** Erstellung der Tabelle shoptyp **
-- ************************************
CREATE TABLE shoptyp (
  typ_nr TINYINT UNSIGNED,
  shoptyp VARCHAR(30),
  PRIMARY KEY (typ_nr)
) ENGINE=INNODB;

-- **********************************
-- ** Erstellung der Tabelle Stadt **
-- **********************************
CREATE TABLE stadt (
  stadt_nr TINYINT UNSIGNED,
  stadt VARCHAR(30),
  lat DOUBLE(9,6),
  lon DOUBLE(9,6),
  PRIMARY KEY (stadt_nr)
) ENGINE=INNODB;
  
-- ******************************************
-- ** Erstellung der Tabelle shop_in_Stadt **
-- ******************************************
CREATE TABLE shop (
  shop_nr SMALLINT UNSIGNED,
  fk_shoptyp TINYINT UNSIGNED,
  strasse VARCHAR(30),
  plz CHAR(5),
  fk_stadt TINYINT UNSIGNED,
  PRIMARY KEY (shop_nr),
  CONSTRAINT fk_shoptyp FOREIGN KEY (fk_shoptyp) 
    REFERENCES shoptyp (typ_nr) 
    ON DELETE CASCADE,
  CONSTRAINT fk_stadt FOREIGN KEY (fk_stadt) 
    REFERENCES stadt (stadt_nr)
    ON DELETE CASCADE
) ENGINE=INNODB;

  
-- ********************************************************
-- ** Die Tabelle auftrag muss das Feld fk_shop_in_stadt **
-- ** aufnehmen,  um die referentielle Integrität        **
-- ** abbilden zu können                                 **
-- ** Bei der referentiellen Integrität macht es Sinn,   **
-- ** bei einer Löschung zu kaskadieren oder den         **
-- ** FOREIGN KEY auf NULL zu setzen, da bei Verwendung  **
-- ** der Option RESTRICT die Löschung einer Stadt oder  **
-- ** eines shoptyps nicht durchgeführt würde.           **
-- ********************************************************
ALTER TABLE auftrag
  ADD fk_shop SMALLINT UNSIGNED,
  ADD CONSTRAINT fk_shop FOREIGN KEY (fk_shop) 
    REFERENCES shop(shop_nr)
    ON DELETE SET NULL ON UPDATE CASCADE;





-- ****************
-- ** AUFGABE 04 **
-- ****************

-- ******************************************************************
-- ** Hinweis: Wegen der referentiellen Integrität ist es wichtig, **
-- ** dass zunächst die Mastertabellen Werte erhalten. Aus diesem  **
-- ** Grunde darf man sich nicht an die in der Aufgabenstellung    **
-- ** vorgegebene Reihenfolge halten!                              **
-- ******************************************************************
INSERT kunde VALUES 
  (NULL, "Claudia", "Schulze", "Hansastr. 334", "20332", "Hamburg", "030", "223234", NULL, 1, .04, NULL),
  (NULL, "Hans", "Mueller", "Hauptstr. 44a", "45147", "Essen", "0201", "345123", 19670520, 1, .03, NULL);

INSERT hersteller VALUES
  (1, "Samsung"),
  (2, "Jamaha");

INSERT kategorie VALUES 
  (1, "Festplatte"),
  (2, "Monitor"),
  (3, "Rechner"),
  (4, "Drucker"),
  (5, "Multimedia"),
  (6, "Frakigkarte"),
  (7, "Mainboard"),
  (8, "Software"),
  (9, "Software (Anwendung)"),
  (10, "Software (Spiel)"),
  (11, "Testkategorie");

INSERT shoptyp VALUES
  (1, "Grosshandel"),
  (2, "Supermarkt"),
  (3, "Einzelhandel");

INSERT stadt VALUES
  (1, "Hamburg", 10, 53.55),
  (2, "Duesseldorf", 6.7667, 51.2167),
  (3, "Dortmund", 4.4734, 51.5147);

INSERT shop VALUES
  (1, 3, "Hauptstr. 38",44145, 3),
  (2, 1, "Lessinggasse 15", 40589, 2);

INSERT auftrag VALUES
  (1000, 20130701, 20130802, 20130831, NULL, "1", "1", 2),
  (1001, 20130701, 20130719, 20130820, 20130805, "0", "2", 1);

INSERT artikel VALUES 
  (1, "Festplatte 178 GB, 7150 rpm", 133.5, .8, 1, 1),
  (2, "Festplatte 165 GB, 7200 rpm", 128.7, .82, 1, 2),
  (3, 'Monitor 19" TFT XA33-5', 368.99, 11.4, 2, 1);

-- ***************************************************************
-- * Hinweis: Statt der oben aufgeführten Lösung mit Hochkommata *
-- * kann man das "Zoll-Zeichen" auch escapen: Die Zeile sieht   *
-- * dann wie folgt aus:                                         *
-- * (3, "Monitor 19\" TFT XA33-5", 368.99, 11.4, 2, 1);         *
-- ***************************************************************



-- ****************
-- ** AUFGABE 05 **
-- ****************

UPDATE kunde 
  SET vorwahl = "040", geburtsdatum = 19780101
  WHERE kd_nr = 1;

-- ******************************************************************
-- ** Hinweis: Die Angabe des Vor- und Nachnamens sowie des        **
-- ** Wohnortes ist gefährlich, weil alle kunden "Claudia Schulze" **
-- ** aus Hamburg geändert werden würden. Daher wurde in der       **
-- ** WHERE-Bedingung die kundennummer angeben                     **
-- ******************************************************************

UPDATE kategorie
  SET kategoriebezeichnung="Grafikkarten"
  WHERE kat_nr=6;

UPDATE kategorie 
  SET kategoriebezeichnung="Software (Betriebssystem)"
  WHERE kat_nr=8;

UPDATE hersteller
  SET herstellerbezeichnung="YAMAHA"
  WHERE herst_nr=2;



-- ****************
-- ** AUFGABE 06 **
-- ****************

-- ****************************************************************
-- * Hinweis: Je nach verwendeter MYSQL-Version werden            *
-- * Warnungen angezeigt, dass nicht alle Felder Daten enthalten. *
-- * Grund: Das Feld letzte Zugriff (TIMESTAMP) wird nicht        *
-- * mit Daten gefüllt                                            *
-- ****************************************************************

-- ******************************************************************
-- ** Voraussetzung für die unten stehende Lösung ist, dass der    **
-- ** lampp-Ordner im Verzeichnis xampp gemountet wurde.           **
-- ** Die untere Lösung geht davon aus, dass die Import-Dateien im **
-- ** lampp-Verzeichnis unter htdocs und dort in einem Ordner      ** 
-- ** 'importe' abgelegt sind.                                     ** 
-- ******************************************************************
LOAD DATA INFILE '/repo/importe/kundenimp.txt' 
INTO TABLE kunde;
  
LOAD DATA INFILE '/repo/importe/herstellerimp.txt' 
INTO TABLE hersteller;
    
LOAD DATA INFILE '/repo/importe/artikelimp.txt' 
INTO TABLE artikel;

LOAD DATA INFILE '/repo/importe/stadtimp.txt' 
INTO TABLE stadt;

LOAD DATA INFILE '/repo/importe/shopimp.txt' 
INTO TABLE shop;

LOAD DATA INFILE '/repo/importe/auftragimp.txt' 
INTO TABLE auftrag;



-- ****************
-- ** AUFGABE 07 **
-- ****************

CREATE TABLE bestellposition (
  fk_auftrag INT UNSIGNED, 
  position TINYINT UNSIGNED,
  fk_artikel SMALLINT UNSIGNED,
  anzahl SMALLINT UNSIGNED,
  CONSTRAINT fk_auftrag FOREIGN KEY (fk_auftrag) 
    REFERENCES auftrag (auft_nr)
    ON UPDATE CASCADE ON DELETE SET NULL,
  CONSTRAINT fk_artikel FOREIGN KEY (fk_artikel) 
    REFERENCES artikel (art_nr)
    ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=INNODB;

LOAD DATA INFILE '/repo/importe/bestellungimp.txt' 
INTO TABLE bestellposition;

UPDATE artikel
  SET einzelpreis = einzelpreis * .94
  WHERE fk_hersteller = 6;

-- **************************************************************
-- * Hinweis: Es ist möglich, dass einige Warnungen ausgegeben  *
-- * werden, da das Feld einzelpreis als DOUBLE(5,2) definiert *
-- * wurde und durch die Multiplikation mit 0.94 mehr als drei  *
-- * Nachkommstellen enstehen können.                           *
-- **************************************************************

-- ***************************************************************
-- * Hinweis: In der ersten Vorgehensweise hätte man mit unseren *
-- * Möglichkeiten über den Befehl SELECT * FROM hersteller      *
-- * herausfinden müssen, welche herst_nr der Hersteller HP  *
-- * hat, um diesen Wert in die Bedingung einfügen zu können.    *
-- * Man kann die Aufgabe besser wie folgt lösen:                *
-- ***************************************************************
-- ***************************************************************
-- * UPDATE artikel, hersteller                                  *
-- * SET einzelpreis = einzelpreis * .94                         *
-- * WHERE herst_nr = fk_hersteller                          *
-- * AND herstellerbezeichnung = "HP";                           *
-- ***************************************************************
-- ***************************************************************
-- * UPDATE artikel                                              *
-- * SET einzelpreis = einzelpreis * .94                         *
-- * WHERE fk_hersteller =                                       *
-- *       (SELECT herst_nr FROM hersteller WHERE            *
-- *        herstellerbezeichnung = "HP");                       *
-- ***************************************************************



-- ****************
-- ** AUFGABE 08 **
-- ****************
-- **************************************************************
-- * Hinweis: Da bei der referentiellen Integrität die Option   *
-- * ON DELETE CASCADE eingeschaltet wurde, muss man den Befehl *
-- * nicht mit Angabe beider Tabellen schreiben.                *
-- * In diesem speziellen Fall wäre es sogar schädlich, da in   *
-- * keiner der angegebenen kategorien Artikel vorhanden sind,  *
-- * und somit die Löschung einer Kategorie nicht durchgeführt  *
-- * würde.                                                     *
-- **************************************************************

DELETE FROM kategorie
WHERE kat_nr IN (3, 5, 10, 11);

-- ***************************
-- * Alternativ:             *
-- *                         *
-- * DELETE FROM kategorie   *
-- * WHERE kat_nr = 3        *
-- * OR kat_nr = 5           *
-- * OR kat_nr = 10          *
-- * OR kat_nr = 11          *
-- ***************************

-- ******************************************************************
-- * Hinweis: Da der Kunde 1 Aufträge vergeben hat, würden bei      *
-- * Löschung eines Kunden in der Tabelle 'auftrag' die             *
-- * entsprechenden Datensätze in der Tabelle 'auftrag' und der     *
-- * Tabelle 'bestellposition' mit einem NULL-Wert belegt           *
-- * (referentielle Integrität mit der Option ON DELETE SET NULL).  *
-- * Dummerweise hat einer der Aufträge keine Positionen, so dass   *
-- * die Löschung unter Angabe der drei Tabellen ebenfalls nicht    *
-- * funktionieren würde.                                           *
-- * Aus diesem Grund muss die Löschung in drei Schritten erfolgen: *
-- * - Löschung aller Bestelldetails der Aufträge des Kunden        *
-- * - danach Löschung aller Aufträge des Kunden und                *
-- * - danach Löschung des Kunden                                   *
-- * Problem: Wie kann man die Bestelldetails ohne großen Aufwand   *
-- * löschen? Lösung: über ein Sub-Select (Erläuterung im Kapitel   *
-- * Abfragen).                                                     *
-- * Oder aber man löscht den Kunden und danach alle Aufträge und   *
-- * Bestellungen, die im Feld kunde einen NULL-Wert besitzen.      *
-- ******************************************************************


DELETE FROM kunde
WHERE kd_nr = 1;

DELETE FROM auftrag
WHERE fk_kunde IS NULL;

DELETE FROM bestellposition
WHERE fk_auftrag IS NULL;

-- *******************************
-- * Alternativ:                 *
-- *                             *
-- * DELETE FROM bestellposition *
-- * WHERE fk_auftrag IN         *
-- *   (SELECT auft_nr           *
-- *   FROM auftrag              *
-- *   WHERE fk_kunde = 1);      *
-- *                             *
-- * DELETE                      *
-- * FROM auftrag                *
-- * WHERE fk_kunde = 1;         *
-- *                             *
-- * DELETE                      *
-- * FROM kunde                  *
-- * WHERE kd_nr = 1;            *
-- *******************************



-- ****************
-- ** AUFGABE 09 **
-- ****************

ALTER TABLE kunde
ADD INDEX (ort, plz);

ALTER TABLE artikel
ADD INDEX (artikelbezeichnung),
ADD INDEX (einzelpreis);

ALTER TABLE bestellposition
ADD INDEX aufpos (fk_auftrag, position);