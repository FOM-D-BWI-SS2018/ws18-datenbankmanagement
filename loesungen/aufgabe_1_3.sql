-- ********************************************
-- ** Vorbereitung: Setze den SQL-Modus      **
-- ** auf ONLY_FULL_GROUP_BY. Das bedeutet:  **
-- ** ung�ltige GROUP BY Statements sind     **
-- ** nicht mehr m�glich.                    **
-- ** Wird diese Option nicht gesetzt w�re   **
-- ** es beispielsweise m�glich, folgendes   **
-- ** Statement abzusetzen:                  **
-- ** SELECT ort, COUNT(*) FROM kunde;       **
-- ********************************************
SET SESSION SQL_MODE = 'STRICT_ALL_TABLES,ONLY_FULL_GROUP_BY'; 

-- ****************************************
-- ** Vorbereitung: L�sche die Datenbank **
-- ** "bestellungen", wenn Sie existiert **
-- ****************************************
DROP DATABASE IF EXISTS bestellungen;



-- ****************
-- ** AUFGABE 01 **
-- ****************

-- ******************************
-- ** Erstellung der Datenbank **
-- ******************************
CREATE DATABASE bestellungen;

-- *******************************
-- ** Aktivierung der Datenbank **
-- *******************************
USE bestellungen;

-- ***********************************
-- ** Erstellung der Tabelle kunden **
-- ***********************************
CREATE TABLE kunden (
  kd_nr SMALLINT UNSIGNED ZEROFILL AUTO_INCREMENT,
  vorname VARCHAR(30),
  nachname VARCHAR(30),
  strasse VARCHAR(25),
  plz CHAR(5),
  ort VARCHAR(30),
  vorwahl VARCHAR(6),
  telefon VARCHAR(15),
  geburtsdatum DATE,
  ledig BOOL,
  PRIMARY KEY (kd_nr)
) ENGINE=INNODB;

-- ************************************
-- ** Erstellung der Tabelle auftrag **
-- ************************************
CREATE TABLE auftrag (
  auft_nr INT UNSIGNED,
  bestelldat DATE,
  lieferdat DATE,
  zahlungsziel DATE,
  zahleingang DATE,
  mahnung ENUM ("0", "1", "2", "3") DEFAULT "0",
  PRIMARY KEY (auft_nr)
) ENGINE=INNODB;

-- ************************************
-- ** Erstellung der Tabelle artikel **
-- ************************************
CREATE TABLE artikel (
  art_nr SMALLINT UNSIGNED,
  artikelbezeichnung VARCHAR(100),
  einzelpreis DOUBLE(8,2),
  gewicht DOUBLE(5,2),
  PRIMARY KEY (art_nr)
) ENGINE=INNODB;

-- ***************************************
-- ** Erstellung der Tabelle hersteller **
-- ***************************************
CREATE TABLE hersteller (
  herst_nr TINYINT UNSIGNED,
  herstellerbezeichnung VARCHAR(30),
  PRIMARY KEY (herst_nr)
) ENGINE=INNODB;

-- **************************************
-- ** Erstellung der Tabelle kategorie **
-- **************************************
CREATE TABLE kategorie (
  kat_nr TINYINT UNSIGNED,
  kategoriebezeichnung VARCHAR(30),
  PRIMARY KEY (kat_nr)
) ENGINE=INNODB;



-- ****************
-- ** AUFGABE 02 **
-- ****************

ALTER TABLE kunden
  ADD rabatt DOUBLE(2,2) DEFAULT 0.03,
  ADD letzter TIMESTAMP,
  MODIFY strasse VARCHAR(35),
  RENAME kunde;

-- ******************************************************************
-- ** Hinweis: diese �nderung kann nicht in einem Zug mit den      **
-- ** vorhergehenden �nderungen durchgef�hrt werden, da das        ** 
-- ** Feld "letzter" erst angelegt werden muss.                    **
-- ******************************************************************
ALTER TABLE kunde
  CHANGE letzter letzter_Zugriff TIMESTAMP;




-- ****************
-- ** AUFGABE 03 **
-- ****************

ALTER TABLE auftrag
  ADD fk_kunde SMALLINT UNSIGNED ZEROFILL,
  ADD CONSTRAINT fk_kunde FOREIGN KEY (fk_kunde) 
    REFERENCES kunde (kd_nr)
    ON UPDATE CASCADE ON DELETE SET NULL;

ALTER TABLE artikel
  ADD fk_kategorie TINYINT UNSIGNED,
  ADD fk_hersteller TINYINT UNSIGNED,
  ADD CONSTRAINT fk_kategorie FOREIGN KEY (fk_kategorie) 
    REFERENCES kategorie (kat_nr)
    ON UPDATE CASCADE ON DELETE CASCADE,
  ADD CONSTRAINT fk_hersteller FOREIGN KEY (fk_hersteller) 
    REFERENCES hersteller (herst_nr)
    ON UPDATE CASCADE ON DELETE SET NULL;

-- ************************************
-- ** Erstellung der Tabelle shoptyp **
-- ************************************
CREATE TABLE shoptyp (
  typ_nr TINYINT UNSIGNED,
  shoptyp VARCHAR(30),
  PRIMARY KEY (typ_nr)
) ENGINE=INNODB;

-- **********************************
-- ** Erstellung der Tabelle Stadt **
-- **********************************
CREATE TABLE stadt (
  stadt_nr TINYINT UNSIGNED,
  stadt VARCHAR(30),
  lat DOUBLE(9,6),
  lon DOUBLE(9,6),
  PRIMARY KEY (stadt_nr)
) ENGINE=INNODB;
  
-- ******************************************
-- ** Erstellung der Tabelle shop          **
-- ******************************************
CREATE TABLE shop (
  shop_nr SMALLINT UNSIGNED,
  fk_shoptyp TINYINT UNSIGNED,
  strasse VARCHAR(30),
  plz CHAR(5),
  fk_stadt TINYINT UNSIGNED,
  PRIMARY KEY (shop_nr),
  CONSTRAINT fk_shoptyp FOREIGN KEY (fk_shoptyp) 
    REFERENCES shoptyp (typ_nr) 
    ON DELETE CASCADE,
  CONSTRAINT fk_stadt FOREIGN KEY (fk_stadt) 
    REFERENCES stadt (stadt_nr)
    ON DELETE CASCADE
) ENGINE=INNODB;

  
-- ********************************************************
-- ** Die Tabelle auftrag muss das Feld fk_shop          **
-- ** aufnehmen, um die referentielle Integrit�t         **
-- ** abbilden zu k�nnen.                                **
-- ** Bei der referentiellen Integrit�t macht es Sinn,   **
-- ** bei einer L�schung zu kaskadieren oder den         **
-- ** FOREIGN KEY auf NULL zu setzen, da bei Verwendung  **
-- ** der Option RESTRICT die L�schung einer Stadt oder  **
-- ** eines Shoptyps nicht durchgef�hrt w�rde.           **
-- ********************************************************
ALTER TABLE auftrag
  ADD fk_shop SMALLINT UNSIGNED,
  ADD CONSTRAINT fk_shop FOREIGN KEY (fk_shop) 
    REFERENCES shop(shop_nr)
    ON DELETE SET NULL ON UPDATE CASCADE;



