-- Einfuegen in SQL: \. path/to/file

-- ********************************************
-- ** Vorbereitung: Setze den SQL-Modus      **
-- ** auf ONLY_FULL_GROUP_BY. Das bedeutet:  **
-- ** ung�ltige GROUP BY Statements sind     **
-- ** nicht mehr m�glich.                    **
-- ** Wird diese Option nicht gesetzt w�re   **
-- ** es beispielsweise m�glich, folgendes   **
-- ** Statement abzusetzen:                  **
-- ** SELECT ort, COUNT(*) FROM kunde;       **
-- ********************************************
SET SESSION SQL_MODE = 'STRICT_ALL_TABLES,ONLY_FULL_GROUP_BY'; 

-- ********************************************
-- ** Vorbereitung: L�sche die Datenbank     **
-- ** "bestellungen", wenn Sie existiert     **
-- ********************************************
DROP DATABASE IF EXISTS bestellungen;



-- ****************
-- ** AUFGABE 01 **
-- ****************

-- ******************************
-- ** Erstellung der Datenbank **
-- ******************************
CREATE DATABASE bestellungen;

-- *******************************
-- ** Aktivierung der Datenbank **
-- *******************************
USE bestellungen;

-- ***********************************
-- ** Erstellung der Tabelle kunden **
-- ***********************************
CREATE TABLE kunden (
  kd_nr SMALLINT UNSIGNED ZEROFILL AUTO_INCREMENT,
  vorname VARCHAR(30),
  nachname VARCHAR(30),
  strasse VARCHAR(25),
  plz CHAR(5),
  ort VARCHAR(30),
  vorwahl VARCHAR(6),
  telefon VARCHAR(15),
  geburtsdatum DATE,
  ledig BOOL,
  PRIMARY KEY (kd_nr)
) ENGINE=INNODB;

-- ************************************
-- ** Erstellung der Tabelle auftrag **
-- ************************************
CREATE TABLE auftrag (
  auft_nr INT UNSIGNED,
  bestelldat DATE,
  lieferdat DATE,
  zahlungsziel DATE,
  zahleingang DATE,
  mahnung ENUM ("0", "1", "2", "3") DEFAULT "0",
  PRIMARY KEY (auft_nr)
) ENGINE=INNODB;

-- ************************************
-- ** Erstellung der Tabelle artikel **
-- ************************************
CREATE TABLE artikel (
  art_nr SMALLINT UNSIGNED,
  artikelbezeichnung VARCHAR(100),
  einzelpreis DOUBLE(8,2),
  gewicht DOUBLE(5,2),
  PRIMARY KEY (art_nr)
) ENGINE=INNODB;

-- ***************************************
-- ** Erstellung der Tabelle hersteller **
-- ***************************************
CREATE TABLE hersteller (
  herst_nr TINYINT UNSIGNED,
  herstellerbezeichnung VARCHAR(30),
  PRIMARY KEY (herst_nr)
) ENGINE=INNODB;

-- **************************************
-- ** Erstellung der Tabelle kategorie **
-- **************************************
CREATE TABLE kategorie (
  kat_nr TINYINT UNSIGNED,
  kategoriebezeichnung VARCHAR(30),
  PRIMARY KEY (kat_nr)
) ENGINE=INNODB;



-- ****************
-- ** AUFGABE 02 **
-- ****************

ALTER TABLE kunden
  ADD rabatt DOUBLE(2,2) DEFAULT 0.03,
  ADD letzter TIMESTAMP,
  MODIFY strasse VARCHAR(35),
  RENAME kunde;

-- ******************************************************************
-- ** Hinweis: diese �nderung kann nicht in einem Zug mit den      **
-- ** vorhergehenden �nderungen durchgef�hrt werden, da das        ** 
-- ** Feld "letzter" erst angelegt werden muss.                    **
-- ******************************************************************
ALTER TABLE kunde
  CHANGE letzter letzter_Zugriff TIMESTAMP;

