create table kunde (
  kunde_id int unsigned auto_increment,
  nachname varchar(30) not null,
  primary key(kunde_id)
);

create table konto (
  konto_id varchar (15),
  kontostand double not null,
  fk_kunde int unsigned,
  primary key (konto_id),
  foreign key (fk_kunde) references kunde(kunde_id)
    on update cascade on delete set null
);

insert into kunde values(default, 'wanders');
insert into konto values ('123', 123456.0, 1);

update konto 
  set kontostand=23456.0 
  where fk_kunde=(select kunde_id from kunde where nachname='wanders');

insert into kunde values(default, 'duerkop');
insert into konto values ('456', 10.0, (select kunde_id from kunde where nachname='duerkop'));

select * from konto join kunde on fk_kunde = kunde_id;

start transaction;

update konto
  set kontostand = kontostand - 100
  where fk_kunde = (select kunde_id from kunde where nachname='duerkop');

update konto
  set kontostand = kontostand + 100
  where fk_kunde = (select kunde_id from kunde where nachname='wanders');

rollback; -- works like charm


start transaction;

update konto
  set kontostand = kontostand - 1000
  where fk_kunde = (select kunde_id from kunde where nachname='wanders');

update konto
  set kontostand = kontostand + 1000
  where fk_kunde = (select kunde_id from kunde where nachname='duerkop');

commit; -- rollback afterwards has no changes, so it works!