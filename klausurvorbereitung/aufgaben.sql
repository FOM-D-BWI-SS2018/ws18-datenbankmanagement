-- Aufgabe 1:
create database bestellungen2;

create table kunde (
  kd_nr smallint unsigned zerofill auto_increment,
  vorname varchar(30),
  nachname varchar(30),
  strasse varchar(25),
  plz char(5),
  ort varchar(25),
  vorwahl char(5),
  telefon varchar(10),
  geburtsdatum date,
  ledig bool,
  primary key (kd_nr)
);

create table auftrag (
  auft_nr int unsigned,
  bestelldat date,
  lieferdat date,
  zahlungsziel date,
  zahleingang date,
  mahnung enum("0", "1", "2", "3") default "0",
  primary key (auft_nr)
);

create table artikel(
  art_nr smallint unsigned,
  artikelbezeichnung varchar(100),
  einzelpreis double(8,2),
  gewicht double(5,2),
  primary key (art_nr)
);

create table hersteller (
  herst_nr tinyint unsigned,
  herstellerbezeichnung varchar(30),
  primary key (herst_nr)
);

create table kategorie (
  kat_nr tinyint unsigned,
  kategoriebezeichnung varchar(30),
  primary key(kategoriebezeichnung)
);

-- Aufgabe 2:
alter table kunde
  add rabatt float(3,2) default 0.03,
  add letzter timestamp;

alter table kunde
  modify strasse varchar(35);

alter table kunde
  change rabatt rabatt double(3,2);

alter table kunde
  change letzter letzer_zugriff timestamp;

alter table kunden
  rename to kunde;

-- Aufgabe 3:
alter table auftrag
  add fk_kunde smallint unsigned;

alter table auftrag
  add foreign key (fk_kunde) references kunde(kd_nr)
  on update cascade on delete set null;

alter table artikel
  add fk_kategorie tinyint unsigned;

alter table artikel
  add foreign key (fk_kategorie) references kategorie(kat_nr)
  on update cascade on delete cascade;

alter table artikel 
  add fk_hersteller tinyint unsigned;

alter table artikel
  add foreign key(fk_hersteller) references hersteller(herst_nr)
  on update cascade on delete set null;

create table stadt(
  stadt_nr tinyint unsigned,
  stadt varchar(30),
  lat double(9,6),
  lon double(9,6),
  primary key (stadt_nr)
);

create table shoptyp(
  typ_nr tinyint unsigned,
  shoptyp varchar(30),
  primary key(typ_nr)
);

create table shop (
  shop_nr smallint unsigned,
  fk_shoptyp tinyint unsigned,
  strasse varchar(30),
  plz char(5),
  fk_stadt tinyint unsigned,
  primary key(shop_nr),
  foreign key (fk_shoptyp) references shoptyp(typ_nr) on delete cascade,
  foreign key (fk_stadt) references stadt(stadt_nr) on delete cascade
);

alter table auftrag
  add fk_shop smallint unsigned;

alter table auftrag
  add foreign key (fk_shop) references shop(shop_nr);

-- Aufgabe 4:

insert into kunde set
  vorname = "Claudia",
  nachname = "Schulze",
  strasse = "Hansastr. 334",
  plz = "20332",
  ort = "Hamburg",
  rabatt = 0.04,
  ledig = 1,
  vorwahl = "030",
  telefon = "2223234";

-- Aufgabe 5:
update kunde 
  set
    vorwahl = "040",
    geburtsdatum = "1979-01-01"
  where vorname = "Claudia" and nachname = "Schulze" and ort = "Hamburg";

-- Aufgabe 7;
create table bestellposition (
  fk_auftrag int unsigned,
  position int,
  fk_artikel smallint unsigned,
  anzahl smallint,
  foreign key (fk_auftrag) references auftrag(auft_nr) on delete set null,
  foreign key (fk_artikel) references artikel(art_nr) on delete cascade
);

-- Aufgabe 8
insert into kategorie values (1, "Test");
insert into hersteller values (1, "Hersteller");
insert into artikel values (1,"Test",2.03,2.0,1,1);
delete from kategorie where kategoriebezeichnung = "Test"; -- durch die optionen der referentiellen integrität werden alle artikel dieser kategorie gelöscht;

-- Aufgabe 9
alter table kunde
  add index (ort, plz);

alter table artikel
  add index (artikelbezeichnung);

alter table artikel
  add index (einzelpreis);

create index AufPos on bestellposition(fk_auftrag, position);

-- Aufgabe 10 (abwärts sortiert)

-- 54
select auft_nr, count(fk_auftrag) as bestellpositionen from auftrag 
left join bestellposition on fk_auftrag = auft_nr
where auft_nr between 8533 and 8537
group by auft_nr;

-- 52
select max(l.avg) - min(l.avg) from (
  select left(plz,1) plz, avg(datediff(lieferdat, bestelldat)) avg
  from auftrag
  join kunde on kd_nr = fk_kunde
  group by left(plz,1)
) l;

-- 51
select * from (
  select left(plz,1) plz, avg(timestampdiff(year, geburtsdatum ,now())) as avg
  from kunde
  group by left(plz,1)
) a
where a.avg > (select avg(timestampdiff(year, geburtsdatum ,now())) from kunde);

-- 50
select kd_nr, vorname, nachname, timestampdiff(year, geburtsdatum, now()) from kunde;

-- 36
select auft_nr, sum(anzahl * einzelpreis * (1-rabatt)) as umsatz from auftrag
join kunde on kd_nr = fk_kunde
join bestellposition on auft_nr = fk_auftrag
join artikel on art_nr = fk_artikel
where auft_nr between 8000 and 9000
group by auft_nr
having sum(anzahl * einzelpreis * (1-rabatt)) > 1000000;

-- 29
select * from kunde 
where month(geburtsdatum) = month(now() + interval 1 month);

-- 12
select left(plz,1) plz, sum(anzahl * einzelpreis) umsatz
from auftrag
join bestellposition on fk_auftrag = auft_nr
join artikel on art_nr = fk_artikel
join kunde on fk_kunde = kd_nr
group by left(plz,1)
order by umsatz desc;

-- 13
select * from auftrag 
left join bestellposition on fk_auftrag = auft_nr
where fk_auftrag is null;

-- 16
select count(*) from artikel 
join kategorie on fk_kategorie = kat_nr
where kategoriebezeichnung in ('monitor','festplatte','drucker');

-- 17
select kategoriebezeichnung, count(*) "anzahl artikel" from artikel 
join kategorie on fk_kategorie = kat_nr
group by kat_nr, kategoriebezeichnung;

-- 23
select auft_nr, sum(anzahl * einzelpreis) umsatz from auftrag
join bestellposition on fk_auftrag = auft_nr
join artikel on fk_artikel = art_nr
where year(lieferdat) = 2015
group by auft_nr
order by umsatz asc
limit 1;

-- 27
select distinct kd_nr, vorname, nachname from auftrag 
join kunde on fk_kunde = kd_nr
where ort in ('hamburg', 'muenchen') and month(lieferdat) = month(bestelldat);

-- 34 schwer
select t.plz, avg(t.auftragsgewicht) as avg from (
  select left(plz,1) plz, sum(anzahl * gewicht) auftragsgewicht from auftrag
  join bestellposition on fk_auftrag = auft_nr
  join artikel on fk_artikel = art_nr
  join kunde on fk_kunde = kd_nr
  group by auft_nr, left(plz,1)
) t
group by plz
order by avg;
-- alternativ mit durchschnittsrechnung anzahl / gesamt

-- 35
select distinct ort, avg(datediff(lieferdat, bestelldat)) diff from auftrag 
join kunde on fk_kunde = kd_nr
group by ort
order by diff asc
limit 5;

-- 36 normal
select auft_nr, sum(anzahl * einzelpreis * (1-rabatt)) 'umsatz inkl. rabatt' from auftrag
join bestellposition on fk_auftrag = auft_nr
join artikel on fk_artikel = art_nr
join kunde on fk_kunde = kd_nr
where auft_nr between 8000 and 9000
group by auft_nr
having sum(anzahl * einzelpreis * (1-rabatt)) > 1000000;

-- 46
select * from bestellposition where fk_auftrag is null; -- ?

select * from bestellposition left join auftrag on fk_auftrag = auft_nr where auft_nr is null;

-- 47 schwer
select abs(
  ( select sum(anzahl * einzelpreis) from auftrag
    join bestellposition on fk_auftrag = auft_nr
    join artikel on fk_artikel = art_nr
    where fk_shop = 15
  )
  -
  ( select sum(anzahl * einzelpreis) from auftrag
    join bestellposition on fk_auftrag = auft_nr
    join artikel on fk_artikel = art_nr
    where fk_shop = 10
  ) 
); --as 'umsatz differenz';
-- 51 schwer
select left(plz, 1), avg(timestampdiff(year,geburtsdatum, now()))
from kunde group by left(plz, 1)
having  avg(timestampdiff(year,geburtsdatum, now())) > (select avg(timestampdiff(year,geburtsdatum, now())) from kunde);
-- 54
select auft_nr, count(fk_auftrag) from auftrag
left join bestellposition on auft_nr = fk_auftrag
where auft_nr between 8533 and 8537
group by auft_nr;

-- 34
select left(t.plz,1) plz, avg(t.gewicht) from (
  select left(plz,1) plz,  sum(anzahl * gewicht) gewicht from auftrag
  join bestellposition on fk_auftrag = auft_nr
  join artikel on fk_artikel = art_nr
  join kunde on fk_kunde = kd_nr
  group by left(plz, 1), auft_nr
) t
group by t.plz
order by t.gewicht desc;

-- 47
select abs(
  (
    select sum(anzahl * einzelpreis) 
    from auftrag 
    join bestellposition on fk_auftrag = auft_nr
    join artikel on fk_artikel = art_nr 
    where fk_shop = 15) - (select sum(anzahl * einzelpreis) from auftrag join bestellposition on fk_auftrag = auft_nr
  join artikel on fk_artikel = art_nr where fk_shop = 10 )
) as umsatz; 
-- 51
select left(plz, 1) plz from kunde
group by left(plz, 1)
having avg(timestampdiff(year, geburtsdatum, now())) > (select avg(timestampdiff(year, geburtsdatum, now())) from kunde);
-- 52
select max(t.avg) - min(t.avg) from (
  select avg(datediff(lieferdat, bestelldat)) avg, left(plz,1) plz 
  from auftrag 
  join kunde on fk_kunde = kd_nr
  group by left(plz,1))t;

-- 54
select auft_nr, count(fk_auftrag) from auftrag 
left join bestellposition on auft_nr = fk_auftrag
where auft_nr between 8533 and 8537
group by auft_nr;