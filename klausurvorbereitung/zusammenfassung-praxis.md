## Datentypen

| Text                                                         | Ganzzahl            | Kommazahl                                                    | Zeit                                        | Weitere                                       |
| ------------------------------------------------------------ | ------------------- | ------------------------------------------------------------ | ------------------------------------------- | --------------------------------------------- |
| **char(x)**<br />keine Beachtung von groß und kleinschreibung | **tinyint** 1byte   | **float(n,d)**<br />n=anzeigebreite<br />d=dezimalstellen<br /><br />d<=24 | **datetime**                                | **enum**("","", ...)                          |
| **varchar(x)**<br />keine Beachtung von groß und kleinschreibung | **smallint** 2byte  | **double(n,d)**<br />d<=53                                   | **date**                                    | **bool**<br />abgebildet durch tinyint 1 o. 0 |
|                                                              | **mediumint** 3byte | **decimal(n,d)**<br />zahl wird als zeichenkette abgespeichert | **time**                                    |                                               |
| [binary(x)]                                                  | **int** 4byte       |                                                              | **timestamp**<br />(bei insert oder update) |                                               |
| [varbinary(x)]                                               | **bigint** 8byte    |                                                              |                                             |                                               |

**Beispiel Parameter bei Kommazahlen:**
Definieren Sie eine Dezimalzahl welche maximial 9,99 darstellen kann:
`float(3,2);`

## Spaltenattribute

* **`unique`**
  Eindeutigkeit, nicht automatisch `not null`
* **`NOT NULL`**
  Eingabepflicht, (Standard in mysql ist `NULL` also nullable)
* **`DEFAULT wert`** 
  Standardwert
* **`AUTO_INCREMENT`**
  Automatische Durchnummerierung beginnend bei 1, für alle ganzzahligen Felder
* **`unsigned`**
  nur positive Werte

---

### Tabelle anlegen

Einfachste Form: 

```mysql
create table tabellenname(
    feldname datentyp [sp_att],
    ...
);
```

Weitere Details bei der referentiellen Integrität!

```mysql
drop table tabellenname; -- Löscht die Tabelle wieder
```

---

### Daten eingeben

```mysql
insert into tabellenname values (1, 'hallo', 3) -- es müssen ALLE Spalten angegeben werden

insert into tabellenname (spalte1, spalte3) values (wert_s_1, wert_s_3);

insert into tabellenname set
	spalte1 = 3,
	spalte5 = 7;
```

---

### Daten ändern

```mysql
update tabelle
	set spalte = wert -- auch berechnung wie einzelpreis * 0.95 möglich
	where art_nr = 55;
```

---

## Strukturänderung

```mysql
alter table tabellenname
	-- Es exitstieren verschiedene Änderungen:
	
	add feldname datentyp spaltenattribute; -- Feldhinzufügen (mehrere durch , Trennung) 
	drop feldname; -- Feld löschen:
	
	add primary key (felder, ...); 	-- Primärschlüssel hinzufügen	
	drop primary key; 	-- Primärschlüssel löschen:
	

	add index (felder, ...);	-- Index hinzufügen
	add index indexname (felder, ...); 	-- Index mit name hinzufügen
	drop index indexname;	-- Index löschen
	
	add unique (felder, ...); 	-- Unique setzen

	alter feld set default wert;	-- Feld-Standardwert ändern
	alter feld drop default;	-- Standardwert löschen
	
	change feld_aktuellername feld_neuername neuer_datentyp;	-- Feld umbenennen
	
	modify feld neuer_datentyp sp_att;	-- Datentyp ändern
	
	rename [to] neuer_name;	-- Tabelle umbennen;
```

## Referentielle Integrität

##### Anforderungen

- INNODB
- Parent-Tabelle braucht eindeutigen Index (`primary key` oder `unique`)
- Verknüpfungsfelder brauchen gleiche Definition
- Verknüpfungsfeld in Child-Tabelle als `foreigen key` mit Refernz auf den eindeutigen Parent-Index 

##### SQL

```mysql
-- Durch create table
create table tabellenname(
    primärschlüsselfeld datentyp [sp_att],
    ...,
    fk_feld fk_datentyp [fk_sp_att],
    primary key (primärschlüsselfeld),
    foreigen key (fk_feld) references tabelle2(id)
    	on update cascade | restrict | set null 	-- muss nicht angegeben werden
    	on delete cascade | restrict | set null		-- muss nicht angegeben werden
);

-- Durch alter table (nach Tabellenerstellung)
alter table tabellenname
	add foreigen key (fk_feld) references tabelle2(id)
    	on update cascade | restrict | set null 	-- muss nicht angegeben werden
    	on delete cascade | restrict | set null;	-- muss nicht angegeben werden

```

## Löschen von Daten

```mysql
-- Einfaches löschen 
delete from tabellename 
	where bedingung;
    
-- Löschen von Daten aus mehreren Tabellen ohne aktivierte referentielle Integrität
delete tabelle1, tabelle2, tabelle3, .. 
	from tabelle1, tabelle2, tabelle3, .. 
	where tb1.pk = tb2.fk 
		and tb2.pk = tb3.fk
		and bedingung
```

## Abfragen

Grundlegenden Abfrage-**Operationen**:

- **Projektion** (`select`)
  Auswahl von Spalten
- **Join** (`from, join`)
  Zusammenfassen mehrere Tabellen
- **Selektion** (`where`)
  Auswahl bestimmter Datensätze nach Kriterien

----

### **Join:**

* **`(inner) join` **
  Schnittmenge

* **`left join`**
  Schnittmenge + komplette Tabelle links von join

* **`right join`**
  Schnittmenge + komplette Tabelle rechts von join

* **"self" join**

  ```mysql
  -- Mitarbeiter Vorgesetzter Problem:
  select ma.name, vo.name from mitarbeiter ma
  join mitarbeiter vo on ma.id = vo.vorgesetzter;
  
  -- Duplikatsuche
  select * from mitarbeiter ma
  join mitarbeiter dup on ma.id != dup.id
  where ma.name = dup.name and .......;
  ```

* **cross join**
  Jeder Datensatz der einen Tabelle wird mit jedem Datensatz der anderen in Verbindung gebracht (karthesisches Produkt) 

-----

### Projektion

Doppelte Datensätze können in der Projektion durch ein **`select distinct`** vermieden werden.

**Sortierung durch:**

```mysql
order by feld | spaltennummer [asc|desc], feld | spaltennummer [asc|desc], ....
```

**Projektion einschränken:**

``````mysql
limit n -- die ersten n Zeilen werden ausgewählt

limit off, n 
-- offset im ResultSet (z.B.: ab Zeile 3) von dort werden dann n Zeilen ausgewählt. 
``````



---

### Selektion

**Selektionsoperatoren bei `where`:**

* `IN ("", "", "")` oder `IN (select * from xyz)`
* `NOT bedingung` oder `!=` negiert.
* `between 1 and 10` prüft ob Wert zwischen den Zahlen liegt (inkl. dieser Zahlen selbst)
* `LIKE `
  Platzhalter:
  `%` beliebig viele beliebige Zeichen
  `_` ein beliebiges Zeichen
* `IS NULL` Prüft ob ein Feld keinen Wert enthält.

---

### Gruppierung

Datensätze zu Gruppen zusammenfassen.

Ohne `group by` werden alle Datensätze als eine Gruppe zusammengefasst und darauf die Gruppenfunktionen angewandt.

Mit `group by` können einzelne Gruppen definiert werden. Gruppe = Die Datensätze wo das group by ergebnis gleich ist. 

Alle Felder der Projektion müssen im `group by` stehen, außer die in Gruppenfunktionen verwendeten.

Das `group by` kommt nach dem `where` in der Syntax.

`having` statt `where` nutzen, wenn über die Ergebnisse von Gruppenfunktionen eingeschränkt wird!

##### Gruppenfunktionen

* `COUNT(feld)` Zählen, * zählt auch NULL-Werte, feld-Angabe nicht.
* `COUNT(distinct feld)` Zählen ohne Duplikate (nur Unterschiedliche Datensätze)
* `SUM(feld)` nur auf Zahlenfelder
* `AVG(feld)` nur auf Zahlenfelder
* `MAX(feld)`
* `MIN(feld)`
* `GROUP_CONCAT(feld [ORDER BY feld] [SEPERATOR "-"])` 
  Konkatenation der Datensätze mit Komma -> ResultSet ist einzeilig 200, 100, ...

---

### Funktionen

##### Text-Funktionen

* `LEFT(zeichenkette, n)` Gibt die linken *n* Zeichen des Texts aus
* `RIGHT(zeichenkette, n)` Gibt die rechten *n* Zeichen des Texts aus

##### Mathematische Funktionen

* `MOD(n,m)` Restwert der Division von *n* durch *m*
* `ABS(zahl)` 

##### Datum / Zeitfunktionen

* `YEAR(date)` Gibt das Jahr des date's zurück.
* `QUARTER(date)` Gibt das Quartal des date's zurück.
* `MONTH(date)` Gibt den Monat des date's zurück.
* `DAY(date)` Gibt den Tag des date's zurück.
* `WEEKDAY(date)` Gibt den Tag  des date's als Zahl ab 0=Montag zurück.
* `DAYOFWEEK(date)` Gibt den Tag  des date's als Zahl ab 1=Sonntag zurück.
* `DAYNAME(date)` Gibt den Tag des date's als englischen Text zurück.
* `DATE_ADD(date, interval)` & `adddate(date,interval)`& `date + interval` Addieren eine Zeit auf ein Datum
  * `INTERVAL n MICROSECOND|SECOND|MINUTE|HOUR|DAY|WEEK|MONTH|QUARTER|YEAR`
* `DATEDIFF(date1,date2)` Differenz in Tagen.  date1 < date2 ? return = negativ
* `CURDATE()` & `CURTIME()` & `NOW()` Geben jeweils die aktuellen Dinge zurück

---

### Sub-Selects

Sub Selects stehen immer in Klammern 

``````mysql
-- Sub Select als Kriterium
select * from table1 where x = (select y from table2 where id = 1);
select * from table1 where x IN (select y from table2); -- IN geht auch hier

-- Sub Select im From Statement IMMMER mit Alias
select b.bauer from (select y as bauer from table2 where id = 1) b; -- Alias hier hinten

-- Sub Select in der Projektion
select datediff((select max(geburtstag) from table1), (select min(geburtstag) from table1));
``````

---

### Transaktionen

**Transaktion** = Anweisungen die logisch zusammengehören.

##### ACID

* **Atomicity**
  Entweder alle Schritte ausführen oder keinen
* **Consistency**
  NACH allen Schritten sind Daten konsistent
* **Isolation**
  Transaktionen dürfen sich nicht gegenseitig beeinflussen
* **Durability**
  Transaktion darf durch Systemausfall nciht verloren gehen

``````mysql
start transaction;
-- befehl 1
-- befehl 2
-- befehl 3
commit; -- Persistiert transaktion. Nur machen wenn alles gut ist, ansonsten:
rollback; -- Spielt alle Änderungen wieder zurück.
``````

**Savepoints:**

``````mysql
start transaction;
-- befehl 1
savepoint p1;
-- befehl 2
-- ..
rollback to savepoint p1;
-- befehl n
commit; -- Persistiert transaktion, ist hier jetzt pflicht.
``````

---

### Ausführungsreihenfolge

| Syntax                                                       | Server                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1. SELECT<br />2. FROM..JOIN..ON<br />3. WHERE<br />4. GROUP BY<br />5. HAVING<br />6. ORDER BY<br />7. LIMIT | 1. FROM..JOIN..ON<br />2. WHERE<br />3. GROUP BY<br />4. HAVING<br />5. SELECT<br />6. ORDER BY<br />7. LIMIT |

---

### Hinweise

- Kein Alias in der Selektion `where`
- Nicht-Gruppenfunktion Attribute immer ins `group by` wenn Sie benötigt werden 

